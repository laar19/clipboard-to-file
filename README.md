# Cliplit

## Description
Based gui application that copy every change from clipboard to a text file

## Luis Acevedo  <laar@pm.me>

## Binaries and installers
https://sourceforge.net/projects/clipboard-to-file/

Alternative link https://www.mediafire.com/folder/uc4465lr03x02/Clipboard_to_file

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## License
GNU General Public License, version 3   
GPLv3

## Project status
Stable

## Technical information:
- Developed in python 3.8
- PySide2 as graphic library
		
## Edit and testing:
### Anaconda
1.- Clone or download this repository   
2.- Creating a new virtual environment: __conda env create -n    env_name -f environment.yml__   
3.- Activate the environment   
4.- Launch the application: __python main.py__   

## Pyinstaller builds
### Linux and windows
1.- pyinstaller --noconsole --icon=ui/resources/img/icon.ico main.py   
2.- Copy __/ui__ folder into __main__ folder generated   
